### Author : Giruba Beulah SE
### Console application in C# that constructs a DLL from the leaves of the Binary Search Tree

1. Node.cs - > Basic Node with the data, left and right pointers
2. BinaryTree.cs -> Has all the functionalities, construction of BST, extraction of leaves and
					construction of DLL to its printing
3. Program.cs   -> The Entry point program for this application